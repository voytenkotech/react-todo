import React from 'react';

import './SearchInput.css';

export default class SearchPanel extends React.Component {

    state = {
        term: ''
    }

    onSearchChange = (e) => {
        const term = e.target.value
        this.setState({ term });
        this.props.onSearchChange(term);
    }
    render() {
        return (
            <div>
                <input 
                type="text"
                placeholder="Поиск"
                value={this.state.term} 
                onChange={this.onSearchChange}
                />
            </div>
    
        )
    }
}