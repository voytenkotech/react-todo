import React from 'react';

import "./AddItem.css"

export default class AddItem extends React.Component {

    state = {
        label: ''
    }
    onLabelChange = (e) => {
        this.setState({
            label: e.target.value
        });
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.props.onAdd(this.state.label);
        this.setState({
            label: ''
        })
    }
    render() {

        return (
            <form className="add-panel d-flex"
                onSubmit={this.onSubmit}>
                <input 
                    type="text"
                    className="input-block form-control" 
                    placeholder="Поднять депо"
                    onChange={this.onLabelChange} 
                    value={this.state.label}/>
                <button 
                    className="btn btn-outline-success"
                >
                    Add
                </button>
            </form>
        )
    }
}
 